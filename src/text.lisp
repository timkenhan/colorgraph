; Graph-Coloring Program
; Using implementation of backtracking algorithm
; Written by: Timothy Kenno Handojo

; Lab 3
; CS 441 Winter 2017
; Portland State University

; Description:
; This program loads a text file containing data in lisp format
; containing number of vertices followed by adjacency matrix.
; It then outputs possible configurations for constraint-compliant
; color configurations.

; This file contains the file I/O functionalities



(DEFUN symetric (adj_matrix n)
       "Matrix symetry checking function:
       Return T when argument is a symetric adjacency matrix,
       or NIL otherwise."
       (DOTIMES (i n) (DOTIMES (j (- n i))
       ; i goes from 0 to n, j goes from i to n
             (UNLESS (= ; check for equality (i.e. interchangability)
                 (AREF adj_matrix i (+ i j))
                 (AREF adj_matrix (+ i j) i)
                     ) ; for row and column
                   (RETURN-FROM symetric NIL)
             ) ; return NIL if inequality is detected
       ) ) T ; reaching here means adj_matrix is symetric
)


(DEFUN load_file (filename)
       "File reading function:
       This function reads thru the input file given by the
       filename string argument."
       (LET ( buffer (filestream (OPEN filename)) adj_matrix n )
            (SETF n (READ filestream)) (READ-LINE filestream)
            (SETF adj_matrix (MAKE-ARRAY (LIST n n)))
            (DOTIMES (i n) (SETF buffer (READ-LINE filestream))
                (DOTIMES (j n) (SETF (AREF adj_matrix i j)
                    (DIGIT-CHAR-P (AREF buffer j))
                ) )
            )
            (CLOSE filestream)
            (UNLESS (symetric adj_matrix n) (ERROR "Assymetric matrix") )
       (LIST adj_matrix n) ) ; ensure matrix symetry for consistency
) ; the adjacency matrix itself is pivoted on the row


(DEFUN save_file (filename adj_matrix n)
       "File writing function:
       This function saves adj_matrix to filename"
       (LET ( (filestream (OPEN filename :direction :output)) )
            (FORMAT filestream "~d~%~%" n)
            (DOTIMES (i n) (DOTIMES (j n)
                (FORMAT filestream "~d" (AREF adj_matrix i j))
            ) (FORMAT filestream "~%") )
       (CLOSE filestream) )
)


(DEFUN gen_matrix (N)
       "Generation function:
       This function randomly generate adjacency matrix.
       Returns two-dimensional array of boolean values."
       (LET ( (adj_matrix (MAKE-ARRAY(LIST N N))) )
            (DOTIMES (i N) ; row i
                 (DOTIMES (j i)
                     (SETF (AREF adj_matrix i j) (AREF adj_matrix j i))
                 ) ; col 0 to i-1
                 (SETF (AREF adj_matrix i i) 0) ; col i
                 (LOOP for j from (1+ i) to (1- N) DO
                       (SETF (AREF adj_matrix i j) (RANDOM 2))
                 ) ; col i+1 to N-1
            )
       adj_matrix )
)

