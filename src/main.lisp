; Graph-Coloring Program
; Using implementation of backtracking algorithm
; Written by: Timothy Kenno Handojo

; Lab 3
; CS 441 Winter 2017
; Portland State University

; Description:
; This program loads a text file containing data in lisp format
; containing number of vertices followed by adjacency matrix.
; It then outputs possible configurations for constraint-compliant
; color configurations.

; This file contains the main (argument-based) interface


(DEFUN display_adjacency (color adj_matrix n)
       (FORMAT T "  ")
       (DOTIMES (j n) (FORMAT T "~c " (AREF color j)) )
       (FORMAT T "~%")
       (DOTIMES (i n) (FORMAT T "~c " (AREF color i)) (DOTIMES (j n)
               (FORMAT T "~d " (AREF adj_matrix i j) )
       ) (FORMAT T "~%") )
)


(DEFUN translate (vertices color_codes n)
       (LET ((temp (MAKE-ARRAY n))) (DOTIMES (i n)
           (SETF
             (AREF temp i)
             (AREF (NTH (AREF vertices i) color_codes) 0)
           )
       ) temp )
)


(DEFUN start_coloring (filename &rest color_codes)
       (FORMAT T "Timothy K Handojo~%")
       (FORMAT T "Lab 3~%")
       (FORMAT T "~a~%" filename)
       (LET* (
           (colors (LENGTH color_codes))
           (temp (load_file filename))
           (adj_matrix (first temp)) (n (second temp))
           (result (color_graph adj_matrix n colors))
           (colored_vertices (translate (FIRST result) color_codes n))
             ) ; initiate these values above then call the function below
               (display_adjacency colored_vertices adj_matrix n)
       )
)

