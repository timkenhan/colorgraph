; Graph-Coloring Program
; Using implementation of backtracking algorithm
; Written by: Timothy Kenno Handojo

; Lab 3
; CS 441 Winter 2017
; Portland State University

; Description:
; This program loads a text file containing data in lisp format
; containing number of vertices followed by adjacency matrix.
; It then outputs possible configurations for constraint-compliant
; color configurations.

; This file contains the core functionalities backtracking algorithm



(DEFUN constraint (conf index adj_matrix)
       "Constraint checking function:
       This function checks index-th element of conf for constraint.
       It returns T if satisfied, or NIL otherwise"
       (DOTIMES (i index)
             (WHEN (AND ; violation check:
                     (/= index i) ; for every vertex other than index-th
                     ; check if this vertex is adjacent to index-th
                     (= 1 (AREF adj_matrix index i))
                     ; if so check for color equality (i.e. violation)
                     (= (AREF conf index) (AREF conf i))
                   ) (RETURN-FROM constraint NIL)
             ) ; return NIL if violation is detected
       ) T ; reaching here means no violation (i.e. constraint is satisfied)
)


(DEFUN search_color (conf index adj_matrix n c)
       "Recursive space-search function:
       For the next moves, this function generate a list of child instances
       of conf with all the possible colors for the index-th vertex.
       This list is then iterated thru with each element as the argument for
       the next recursive calls."
       (LET ( (next_move (LIST)) (result (LIST conf)) ) (UNLESS (= index n)
            (SETF result NIL) ; initialize as the placeholder
            ; check if index > 0, if so start looping
            (IF (> index 0) (DOTIMES (color c) ; for each possible color
                (SETF (AREF conf index) color) ; set current conf
                ; check current conf for constraint satisfaction
                (WHEN (constraint conf index adj_matrix) (SETF next_move
                      (APPEND (LIST (COPY-SEQ conf)) next_move)
                ) ) ; append it to next_move when true is returned
            ) (PROGN ; vertex 0 color is always 0 to minimize redundancies
                (SETF (AREF conf index) 0)
                (SETF next_move (LIST(COPY-SEQ conf)) )
            ) )
            ; if there's no next_move, result, which is NIL, is returned
            ; otherwise recurse to each of next_move
            (DOLIST (m next_move) (SETF result (APPEND result
                (search_color (COPY-SEQ m) (1+ index) adj_matrix n c)
            ) ) ) ; then append the return value of recursion into result
       ) result ) ; base case: index=n; conf remains as result and is returned
       ; this var is always returned to allows multiple conf to be returned
)


(DEFUN color_graph (adj_matrix n c)
       "Wrapper function:
       This function calls the load_file with filename as argument.
       The returned data is then used to start the recursive call."
       (LET ((configuration (MAKE-ARRAY n :initial-element NIL)))
           (search_color configuration 0 adj_matrix n c)
       )    ; then use it to start the recursive calls
)



